use rand::distributions::Alphanumeric;
use rand::rngs::OsRng;
use rand::Rng;

pub fn generate_token() -> String {
    OsRng.sample_iter(&Alphanumeric).take(4).collect::<String>()
}

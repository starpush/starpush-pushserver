use std::net::SocketAddr;

use clap::{App, Arg};

pub struct Config {
    listen_address: SocketAddr,
}

impl Config {
    pub fn listen_address(&self) -> SocketAddr {
        self.listen_address
    }
}

pub fn load_config() -> Config {
    let matches = App::new("star push")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .arg(
            Arg::with_name("listen-address")
                .long("listen-address")
                .short("l")
                .env("STAR_PUSH_LISTEN_ADDRESS")
                .takes_value(true)
                .help("IP to listen on"),
        )
        .arg(
            Arg::with_name("port")
                .long("port")
                .short("p")
                .env("STAR_PUSH_PORT")
                .takes_value(true)
                .help("Port to listen on"),
        )
        .get_matches();

    let listen_address = match matches.value_of("ip address") {
        Some(addr) => String::from(addr),
        _ => String::from("0.0.0.0"),
    };

    let listen_port = match matches.value_of("port") {
        Some(port) => String::from(port),
        _ => String::from("8080"),
    };

    let mut listen_address = listen_address;
    listen_address.push_str(":");
    listen_address.push_str(&listen_port);

    let listen_address = match listen_address.parse() {
        Ok(addr) => addr,
        Err(e) => panic!(e),
    };

    Config { listen_address }
}

use log::info;

use async_trait::async_trait;
use log::debug;
use swagger::{ApiError, EmptyContext};

use openapi_client::models::{PushRequest, SubscriptionSet, Version};
use openapi_client::server::MakeService;
use openapi_client::{
    Api, MessageTokenDeleteResponse, PushTokenPostResponse, SubscribePostResponse,
    SubscriptionSetTokenDeleteResponse, SubscriptionSetTokenGetResponse,
    SubscriptionTokenDeleteResponse, SubscriptionTokenGetResponse, VersionGetResponse, API_VERSION,
};

use crate::data_store::mem_store::MemoryStore;

use crate::config::Config;
use crate::data_store::{
    CreatePushMessageResult, CreateSubscriptionResult, DataStore, DeleteResult, GetMessagesResult,
};

const MAX_PAYLOAD_LEN: usize = 4096;

pub struct StarpushServer {
    config: Config,
    data_store: MemoryStore,
}

#[derive(Clone)]
struct Server {
    data_store: MemoryStore,
}

impl StarpushServer {
    pub fn new(config: Config, data_store: MemoryStore) -> Self {
        debug!("Creating server");
        StarpushServer { config, data_store }
    }

    pub async fn start(&self) {
        info!("Starting server");
        let service = MakeService::new(Server {
            data_store: self.data_store.clone(),
        });
        let service =
            openapi_client::server::context::MakeAddContext::<_, EmptyContext>::new(service);

        let server = hyper::server::Server::bind(&self.config.listen_address()).serve(service);
        if let Err(e) = server.await {
            panic!("Error during shutdown {:?}", e);
        }
    }
}

#[async_trait]
impl<C> Api<C> for Server
where
    C: Send + Sync,
{
    async fn message_token_delete(
        &self,
        token: String,
        _context: &C,
    ) -> Result<MessageTokenDeleteResponse, ApiError> {
        debug!("received message_token_delete");

        let result = self.data_store.delete_message(token).await;

        match result {
            DeleteResult::Deleted => Ok(MessageTokenDeleteResponse::Deleted),
            DeleteResult::TokenNotFound => Ok(MessageTokenDeleteResponse::InvalidMessageToken),
            DeleteResult::Error(err) => Err(err.into()),
        }
    }

    async fn push_token_post(
        &self,
        token: String,
        push_message: PushRequest,
        _context: &C,
    ) -> Result<PushTokenPostResponse, ApiError> {
        debug!("received push_token_post");

        if push_message.payload.len() > MAX_PAYLOAD_LEN {
            return Ok(PushTokenPostResponse::PayloadTooLarge);
        }

        let result = self
            .data_store
            .create_push_request(token, push_message)
            .await;

        match result {
            CreatePushMessageResult::Created => Ok(PushTokenPostResponse::Created),
            CreatePushMessageResult::TooManyRequests => Ok(PushTokenPostResponse::TooManyRequests),
            CreatePushMessageResult::TokenNotFound => Ok(PushTokenPostResponse::InvalidPushToken),
            CreatePushMessageResult::Error(err) => Err(err.into()),
        }
    }

    async fn subscribe_post(
        &self,
        subscription_set: Option<SubscriptionSet>,
        _context: &C,
    ) -> Result<SubscribePostResponse, ApiError> {
        debug!("received subscribe_post");

        let token = match subscription_set {
            None => None,
            Some(set) => Some(set.subscription_set_token),
        };
        let result = self.data_store.create_subscription(token).await;

        match result {
            CreateSubscriptionResult::Created(subscription) => {
                Ok(SubscribePostResponse::Created(subscription))
            }
            CreateSubscriptionResult::TooManyRequests => Ok(SubscribePostResponse::TooManyRequests),
            CreateSubscriptionResult::InvalidSubscriptionSet => {
                Ok(SubscribePostResponse::InvalidSubscriptionSet)
            }
            CreateSubscriptionResult::Error(err) => Err(err.into()),
        }
    }

    async fn subscription_set_token_delete(
        &self,
        token: String,
        _context: &C,
    ) -> Result<SubscriptionSetTokenDeleteResponse, ApiError> {
        debug!("received subscription_set_token_delete");

        let result = self.data_store.delete_subscription_set(token).await;

        match result {
            DeleteResult::Deleted => Ok(SubscriptionSetTokenDeleteResponse::Deleted),
            DeleteResult::TokenNotFound => {
                Ok(SubscriptionSetTokenDeleteResponse::InvalidSubscriptionSetToken)
            }
            DeleteResult::Error(err) => Err(err.into()),
        }
    }

    async fn subscription_set_token_get(
        &self,
        token: String,
        _context: &C,
    ) -> Result<SubscriptionSetTokenGetResponse, ApiError> {
        debug!("received subscription_set_token_get");

        let result = self.data_store.get_set_messages(token).await;

        match result {
            GetMessagesResult::Messages(messages) => Ok(
                SubscriptionSetTokenGetResponse::SubscriptionSetMessages(messages),
            ),
            GetMessagesResult::TokenNotFound => {
                Ok(SubscriptionSetTokenGetResponse::InvalidSubscriptionSetToken)
            }
            GetMessagesResult::Error(err) => Err(err.into()),
        }
    }

    async fn subscription_token_delete(
        &self,
        token: String,
        _context: &C,
    ) -> Result<SubscriptionTokenDeleteResponse, ApiError> {
        debug!("received subscription_token_delete");

        let result = &self.data_store.delete_subscription(token).await;

        match result {
            DeleteResult::Deleted => Ok(SubscriptionTokenDeleteResponse::Deleted),
            DeleteResult::TokenNotFound => {
                Ok(SubscriptionTokenDeleteResponse::InvalidSubscriptionToken)
            }
            DeleteResult::Error(err) => Err(ApiError(err.clone())),
        }
    }

    async fn subscription_token_get(
        &self,
        token: String,
        _context: &C,
    ) -> Result<SubscriptionTokenGetResponse, ApiError> {
        debug!("received subscription_token_get");

        let result = self.data_store.get_messages(token).await;

        debug!("handling subscription_token_get result");

        match result {
            GetMessagesResult::Messages(messages) => {
                Ok(SubscriptionTokenGetResponse::SubscriptionMessages(messages))
            }
            GetMessagesResult::TokenNotFound => {
                Ok(SubscriptionTokenGetResponse::InvalidSubscriptionToken)
            }
            GetMessagesResult::Error(err) => Err(err.into()),
        }
    }

    async fn version_get(&self, _context: &C) -> Result<VersionGetResponse, ApiError> {
        debug!("received version_get");

        Ok(VersionGetResponse::ServerApiVersion(Version {
            version: API_VERSION.to_string(),
        }))
    }
}

/// Create a new channel subscribed to the given signals
async fn shutdown_signal() {
    let signals =
        match signal_hook::iterator::Signals::new(&[signal_hook::SIGINT, signal_hook::SIGTERM]) {
            Ok(signals) => signals,
            Err(err) => {
                panic!("Failed to create signal iterator: {}", err);
            }
        };

    for _ in signals.forever() {
        return;
    }
}

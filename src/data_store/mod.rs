use async_trait::async_trait;

use openapi_client::models;
use openapi_client::models::PushRequest;

pub mod mem_store;

#[derive(Debug)]
pub enum CreateSubscriptionResult {
    Created(models::Subscription),
    TooManyRequests,
    InvalidSubscriptionSet,
    Error(String),
}

#[derive(Debug)]
pub enum CreatePushMessageResult {
    Created,
    TokenNotFound,
    TooManyRequests,
    Error(String),
}

#[derive(Debug)]
pub enum GetMessagesResult {
    Messages(Vec<models::PushMessage>),
    TokenNotFound,
    Error(String),
}

#[derive(Debug)]
pub enum DeleteResult {
    Deleted,
    TokenNotFound,
    Error(String),
}

#[async_trait]
pub trait DataStore {
    async fn create_subscription(&self, token: Option<String>) -> CreateSubscriptionResult;
    async fn create_push_request(
        &self,
        token: String,
        request: PushRequest,
    ) -> CreatePushMessageResult;
    async fn delete_message(&self, token: String) -> DeleteResult;
    async fn get_messages(&self, token: String) -> GetMessagesResult;
    async fn get_set_messages(&self, token: String) -> GetMessagesResult;
    async fn delete_subscription(&self, token: String) -> DeleteResult;
    async fn delete_subscription_set(&self, token: String) -> DeleteResult;
}

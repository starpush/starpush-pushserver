use std::collections::HashMap;

use async_trait::async_trait;
use log::debug;
use log::error;
use log::info;

use openapi_client::models::{PushMessage, PushRequest, Subscription, SubscriptionSet};

use crate::data_store::{
    CreatePushMessageResult, CreateSubscriptionResult, DataStore, DeleteResult, GetMessagesResult,
};
use crate::utils::generate_token;

pub enum Command {
    CreateSubscription {
        subscription_set_token: Option<String>,
        sender: tokio::sync::oneshot::Sender<CreateSubscriptionResult>,
    },
    CreatePushMessage {
        push_token: String,
        push_request: PushRequest,
        sender: tokio::sync::oneshot::Sender<CreatePushMessageResult>,
    },
    DeleteMessage {
        message_token: String,
        sender: tokio::sync::oneshot::Sender<DeleteResult>,
    },
    GetMessages {
        subscription_token: String,
        sender: tokio::sync::oneshot::Sender<GetMessagesResult>,
    },
    GetSetMessages {
        subscription_set_token: String,
        sender: tokio::sync::oneshot::Sender<GetMessagesResult>,
    },
    DeleteSubscription {
        subscription_token: String,
        sender: tokio::sync::oneshot::Sender<DeleteResult>,
    },
    DeleteSubscriptionSet {
        subscription_set_token: String,
        sender: tokio::sync::oneshot::Sender<DeleteResult>,
    },
}

#[derive(Clone)]
pub struct MemoryStore {
    channel: tokio::sync::mpsc::Sender<Command>,
}

impl MemoryStore {
    pub fn new() -> MemoryStore {
        let (tx, mut rx) = tokio::sync::mpsc::channel(128);
        tokio::spawn(async move {
            info!("Started Memory Store Handler");
            let mut database = Database::new();

            info!("Waiting for commands");
            while let Some(command) = rx.recv().await {
                debug!("received a command");
                database.handle_command(command).await;
            }
            info!("Finished Memory Store Handler");
        });
        MemoryStore { channel: tx }
    }
}

struct Database {
    push_tokens: HashMap<String, String>,
    subscriptions: HashMap<String, SubscriptionData>,
    subscription_sets: HashMap<String, Vec<String>>,
    messages: HashMap<String, PushMessage>,
}

struct SubscriptionData {
    subscription_set_token: String,
    push_token: String,
    messages: Vec<String>,
}

impl Database {
    fn new() -> Database {
        Database {
            push_tokens: HashMap::new(),
            subscriptions: HashMap::new(),
            subscription_sets: HashMap::new(),
            messages: HashMap::new(),
        }
    }

    async fn handle_command(&mut self, command: Command) {
        info!("Handling command");
        match command {
            Command::CreateSubscription {
                subscription_set_token,
                sender,
            } => {
                // TODO limits?
                info!("Creating a new subscription");

                let subscription_set_token = match subscription_set_token {
                    None => {
                        let mut token = generate_token();
                        while self.subscription_sets.contains_key(&token) {
                            token = generate_token();
                        }
                        token
                    }
                    Some(token) => {
                        if !self.subscription_sets.contains_key(&token) {
                            error!("Invalid subscription set");
                            sender
                                .send(CreateSubscriptionResult::InvalidSubscriptionSet)
                                .unwrap();
                            return;
                        }
                        token
                    }
                };

                let mut subscription_token = generate_token();
                while self.subscriptions.contains_key(&subscription_token) {
                    subscription_token = generate_token();
                }

                let mut push_token = generate_token();
                while self.push_tokens.contains_key(&push_token) {
                    push_token = generate_token();
                }

                self.subscriptions.insert(
                    subscription_token.clone(),
                    SubscriptionData {
                        subscription_set_token: subscription_set_token.clone(),
                        push_token: push_token.clone(),
                        messages: Vec::new(),
                    },
                );
                self.push_tokens
                    .insert(push_token.clone(), subscription_token.clone());
                match self.subscription_sets.get_mut(&subscription_set_token) {
                    None => {
                        self.subscription_sets.insert(
                            subscription_set_token.clone(),
                            vec![subscription_token.clone()],
                        );
                    }
                    Some(subscriptions) => {
                        subscriptions.push(subscription_token.clone());
                    }
                };

                let subscription_set = SubscriptionSet {
                    subscription_set_token,
                };
                let subscription = Subscription {
                    subscription_token,
                    push_token,
                    subscription_set,
                };
                info!("Subscription set created");
                sender
                    .send(CreateSubscriptionResult::Created(subscription))
                    .unwrap();
            }
            Command::CreatePushMessage {
                push_token,
                push_request,
                sender,
            } => match self.push_tokens.get(&push_token) {
                None => {
                    sender.send(CreatePushMessageResult::TokenNotFound);
                }
                Some(subscriptionToken) => {
                    match self.subscriptions.get_mut(subscriptionToken) {
                        None => {
                            error!("Our DB is broken 3");
                        }
                        Some(subscription) => {
                            let message_token = generate_token();
                            self.messages.insert(
                                message_token.clone(),
                                PushMessage {
                                    message_token: message_token.clone(),
                                    payload: push_request.payload,
                                    subscription: subscriptionToken.clone(),
                                },
                            );
                            subscription.messages.push(message_token);
                            sender.send(CreatePushMessageResult::Created);
                        }
                    };
                }
            },
            Command::DeleteMessage {
                message_token,
                sender,
            } => match self.messages.remove(&message_token) {
                None => {
                    sender.send(DeleteResult::TokenNotFound).unwrap();
                }
                Some(PushMessage {
                    message_token: _,
                    payload: _,
                    subscription,
                }) => {
                    match self.subscriptions.get_mut(&subscription) {
                        None => {
                            sender
                                .send(DeleteResult::Error(
                                    "Message references unknown subscription".to_string(),
                                ))
                                .unwrap();
                        }
                        Some(subscription) => {
                            subscription.messages.retain(|elem| elem != &message_token);
                            sender.send(DeleteResult::Deleted).unwrap();
                        }
                    };
                }
            },
            Command::GetMessages {
                subscription_token,
                sender,
            } => match self.subscriptions.get_mut(&subscription_token) {
                None => {
                    sender.send(GetMessagesResult::TokenNotFound).unwrap();
                }
                Some(subscription) => {
                    let mut messages = Vec::new();
                    for message_token in subscription.messages.iter() {
                        match self.messages.get(message_token) {
                            None => {
                                // TODO delete the message token from the subscription
                            }
                            Some(message) => {
                                messages.push(message.clone());
                            }
                        };
                    }
                    sender.send(GetMessagesResult::Messages(messages)).unwrap();
                }
            },
            Command::GetSetMessages {
                subscription_set_token,
                sender,
            } => {
                let mut messages = Vec::new();

                if let Some(subscription_set) = self.subscription_sets.get(&subscription_set_token)
                {
                    for subscription_token in subscription_set.iter() {
                        match self.subscriptions.get_mut(subscription_token) {
                            None => {
                                // TODO this means our db is broken
                                error!("Our DB is broken 1!")
                            }
                            Some(subscription) => {
                                for message_token in subscription.messages.iter() {
                                    match self.messages.get(message_token) {
                                        None => error!("Our DB is broken2!"),
                                        Some(message) => {
                                            messages.push(message.clone());
                                        }
                                    };
                                }
                            }
                        };
                    }
                    sender.send(GetMessagesResult::Messages(messages)).unwrap();
                } else {
                    sender.send(GetMessagesResult::TokenNotFound).unwrap();
                    debug!("Requested messages with invalid set token")
                };
            }
            Command::DeleteSubscription {
                subscription_token,
                sender,
            } => match self.subscriptions.remove(&subscription_token) {
                None => {
                    sender.send(DeleteResult::TokenNotFound).unwrap();
                }
                Some(subscription) => {
                    match self
                        .subscription_sets
                        .get_mut(&subscription.subscription_set_token)
                    {
                        None => { //TODO this means the db is broken
                        }
                        Some(subscription_set) => {
                            subscription_set.retain(|elem| elem != &subscription_token);
                            if subscription_set.is_empty() {
                                self.subscription_sets.remove(&subscription_token);
                                debug!("Subscription set turned empty. removed it");
                            };
                        }
                    }
                    sender.send(DeleteResult::Deleted).unwrap();
                }
            },
            Command::DeleteSubscriptionSet {
                subscription_set_token,
                sender,
            } => match self.subscription_sets.remove(&subscription_set_token) {
                None => {
                    sender.send(DeleteResult::TokenNotFound).unwrap();
                }
                Some(subscription_set) => {
                    for subscription in subscription_set {
                        self.subscriptions.remove(&subscription);
                    }
                    sender.send(DeleteResult::Deleted).unwrap();
                }
            },
        }
    }
}

impl MemoryStore {
    async fn run_command(&self, command: Command) {
        info!("sending command");
        let mut sender = self.channel.clone();
        match sender.send(command).await {
            Ok(_) => info!("Command sent"),
            Err(e) => panic!("Data store can't send commands {:?}", e.to_string()),
        }
    }
}

#[async_trait]
impl DataStore for MemoryStore {
    async fn create_subscription(&self, token: Option<String>) -> CreateSubscriptionResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::CreateSubscription {
            subscription_set_token: token,
            sender: tx,
        })
        .await;
        debug!("Waiting for subscription result");
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command CreateSubscription {}", e);
                CreateSubscriptionResult::Error(e.to_string())
            }
        }
    }

    async fn create_push_request(
        &self,
        token: String,
        request: PushRequest,
    ) -> CreatePushMessageResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::CreatePushMessage {
            push_token: token,
            push_request: request,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command CreatePushMessage {}", e);
                CreatePushMessageResult::Error(e.to_string())
            }
        }
    }

    async fn delete_message(&self, token: String) -> DeleteResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::DeleteMessage {
            message_token: token,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command DeleteMessage {}", e);
                DeleteResult::Error(e.to_string())
            }
        }
    }

    async fn get_messages(&self, token: String) -> GetMessagesResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::GetMessages {
            subscription_token: token,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command GetMessages {}", e);
                GetMessagesResult::Error(e.to_string())
            }
        }
    }

    async fn get_set_messages(&self, token: String) -> GetMessagesResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::GetSetMessages {
            subscription_set_token: token,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command GetSetMessages {}", e);
                GetMessagesResult::Error(e.to_string())
            }
        }
    }

    async fn delete_subscription(&self, token: String) -> DeleteResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::DeleteSubscription {
            subscription_token: token,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command DeleteSubscription {}", e);
                DeleteResult::Error(e.to_string())
            }
        }
    }

    async fn delete_subscription_set(&self, token: String) -> DeleteResult {
        let (tx, rx) = tokio::sync::oneshot::channel();
        self.run_command(Command::DeleteSubscriptionSet {
            subscription_set_token: token,
            sender: tx,
        })
        .await;
        match rx.await {
            Ok(result) => result,
            Err(e) => {
                error!("Failed to run command DeleteSubscriptionSet {}", e);
                DeleteResult::Error(e.to_string())
            }
        }
    }
}

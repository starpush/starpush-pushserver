#![allow(missing_docs, trivial_casts, unused_variables, unused_mut, unused_imports, unused_extern_crates, non_camel_case_types)]

use async_trait::async_trait;
use futures::Stream;
use std::error::Error;
use std::task::{Poll, Context};
use swagger::{ApiError, ContextWrapper};

type ServiceError = Box<dyn Error + Send + Sync + 'static>;

pub const BASE_PATH: &'static str = "";
pub const API_VERSION: &'static str = "0.0.1";

#[derive(Debug, PartialEq)]
#[must_use]
pub enum MessageTokenDeleteResponse {
    /// Deleted
    Deleted
    ,
    /// Invalid message token
    InvalidMessageToken
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum PushTokenPostResponse {
    /// Created
    Created
    ,
    /// Invalid push token
    InvalidPushToken
    ,
    /// Payload too large
    PayloadTooLarge
    ,
    /// Too many requests
    TooManyRequests
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum SubscribePostResponse {
    /// Created
    Created
    (models::Subscription)
    ,
    /// Invalid subscription set
    InvalidSubscriptionSet
    ,
    /// Too many requests
    TooManyRequests
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum SubscriptionSetTokenDeleteResponse {
    /// Deleted
    Deleted
    ,
    /// Invalid subscription set token
    InvalidSubscriptionSetToken
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum SubscriptionSetTokenGetResponse {
    /// Subscription set messages
    SubscriptionSetMessages
    (Vec<models::PushMessage>)
    ,
    /// Invalid subscription set token
    InvalidSubscriptionSetToken
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum SubscriptionTokenDeleteResponse {
    /// Deleted
    Deleted
    ,
    /// Invalid subscription token
    InvalidSubscriptionToken
}

#[derive(Debug, PartialEq)]
#[must_use]
pub enum SubscriptionTokenGetResponse {
    /// Subscription messages
    SubscriptionMessages
    (Vec<models::PushMessage>)
    ,
    /// Invalid subscription token
    InvalidSubscriptionToken
}

#[derive(Debug, PartialEq)]
pub enum VersionGetResponse {
    /// Server api version.
    ServerApiVersion
    (models::Version)
}

/// API
#[async_trait]
pub trait Api<C: Send + Sync> {
    fn poll_ready(&self, _cx: &mut Context) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>> {
        Poll::Ready(Ok(()))
    }

    /// Delete a message that was previously sent
    async fn message_token_delete(
        &self,
        token: String,
        context: &C) -> Result<MessageTokenDeleteResponse, ApiError>;

    /// Request push message delivery
    async fn push_token_post(
        &self,
        token: String,
        push_request: models::PushRequest,
        context: &C) -> Result<PushTokenPostResponse, ApiError>;

    /// create a new push subscription
    async fn subscribe_post(
        &self,
        subscription_set: Option<models::SubscriptionSet>,
        context: &C) -> Result<SubscribePostResponse, ApiError>;

    /// Delete the subscription set and all related subscriptions.
    async fn subscription_set_token_delete(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionSetTokenDeleteResponse, ApiError>;

    /// Request messages belonging to this subscription set.
    async fn subscription_set_token_get(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionSetTokenGetResponse, ApiError>;

    /// Delete the subscription.
    async fn subscription_token_delete(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionTokenDeleteResponse, ApiError>;

    /// Request messages belonging to this subscription.
    async fn subscription_token_get(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionTokenGetResponse, ApiError>;

    /// Return the server api version.
    async fn version_get(
        &self,
        context: &C) -> Result<VersionGetResponse, ApiError>;

}

/// API where `Context` isn't passed on every API call
#[async_trait]
pub trait ApiNoContext<C: Send + Sync> {

    fn poll_ready(&self, _cx: &mut Context) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>>;

    fn context(&self) -> &C;

    /// Delete a message that was previously sent
    async fn message_token_delete(
        &self,
        token: String,
        ) -> Result<MessageTokenDeleteResponse, ApiError>;

    /// Request push message delivery
    async fn push_token_post(
        &self,
        token: String,
        push_request: models::PushRequest,
        ) -> Result<PushTokenPostResponse, ApiError>;

    /// create a new push subscription
    async fn subscribe_post(
        &self,
        subscription_set: Option<models::SubscriptionSet>,
        ) -> Result<SubscribePostResponse, ApiError>;

    /// Delete the subscription set and all related subscriptions.
    async fn subscription_set_token_delete(
        &self,
        token: String,
        ) -> Result<SubscriptionSetTokenDeleteResponse, ApiError>;

    /// Request messages belonging to this subscription set.
    async fn subscription_set_token_get(
        &self,
        token: String,
        ) -> Result<SubscriptionSetTokenGetResponse, ApiError>;

    /// Delete the subscription.
    async fn subscription_token_delete(
        &self,
        token: String,
        ) -> Result<SubscriptionTokenDeleteResponse, ApiError>;

    /// Request messages belonging to this subscription.
    async fn subscription_token_get(
        &self,
        token: String,
        ) -> Result<SubscriptionTokenGetResponse, ApiError>;

    /// Return the server api version.
    async fn version_get(
        &self,
        ) -> Result<VersionGetResponse, ApiError>;

}

/// Trait to extend an API to make it easy to bind it to a context.
pub trait ContextWrapperExt<C: Send + Sync> where Self: Sized
{
    /// Binds this API to a context.
    fn with_context(self: Self, context: C) -> ContextWrapper<Self, C>;
}

impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ContextWrapperExt<C> for T {
    fn with_context(self: T, context: C) -> ContextWrapper<T, C> {
         ContextWrapper::<T, C>::new(self, context)
    }
}

#[async_trait]
impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ApiNoContext<C> for ContextWrapper<T, C> {
    fn poll_ready(&self, cx: &mut Context) -> Poll<Result<(), ServiceError>> {
        self.api().poll_ready(cx)
    }

    fn context(&self) -> &C {
        ContextWrapper::context(self)
    }

    /// Delete a message that was previously sent
    async fn message_token_delete(
        &self,
        token: String,
        ) -> Result<MessageTokenDeleteResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().message_token_delete(token, &context).await
    }

    /// Request push message delivery
    async fn push_token_post(
        &self,
        token: String,
        push_request: models::PushRequest,
        ) -> Result<PushTokenPostResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().push_token_post(token, push_request, &context).await
    }

    /// create a new push subscription
    async fn subscribe_post(
        &self,
        subscription_set: Option<models::SubscriptionSet>,
        ) -> Result<SubscribePostResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().subscribe_post(subscription_set, &context).await
    }

    /// Delete the subscription set and all related subscriptions.
    async fn subscription_set_token_delete(
        &self,
        token: String,
        ) -> Result<SubscriptionSetTokenDeleteResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().subscription_set_token_delete(token, &context).await
    }

    /// Request messages belonging to this subscription set.
    async fn subscription_set_token_get(
        &self,
        token: String,
        ) -> Result<SubscriptionSetTokenGetResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().subscription_set_token_get(token, &context).await
    }

    /// Delete the subscription.
    async fn subscription_token_delete(
        &self,
        token: String,
        ) -> Result<SubscriptionTokenDeleteResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().subscription_token_delete(token, &context).await
    }

    /// Request messages belonging to this subscription.
    async fn subscription_token_get(
        &self,
        token: String,
        ) -> Result<SubscriptionTokenGetResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().subscription_token_get(token, &context).await
    }

    /// Return the server api version.
    async fn version_get(
        &self,
        ) -> Result<VersionGetResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().version_get(&context).await
    }

}


#[cfg(feature = "client")]
pub mod client;

// Re-export Client as a top-level name
#[cfg(feature = "client")]
pub use client::Client;

#[cfg(feature = "server")]
pub mod server;

// Re-export router() as a top-level name
#[cfg(feature = "server")]
pub use self::server::Service;

#[cfg(feature = "server")]
pub mod context;

pub mod models;

#[cfg(any(feature = "client", feature = "server"))]
pub(crate) mod header;

#![allow(unused_qualifications)]

use crate::models;
#[cfg(any(feature = "client", feature = "server"))]
use crate::header;


/// the payload to transmit
#[derive(Debug, Clone, PartialEq, PartialOrd, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct Payload(String);

impl std::convert::From<String> for Payload {
    fn from(x: String) -> Self {
        Payload(x)
    }
}

impl std::string::ToString for Payload {
    fn to_string(&self) -> String {
       self.0.to_string()
    }
}

impl std::str::FromStr for Payload {
    type Err = std::string::ParseError;
    fn from_str(x: &str) -> std::result::Result<Self, Self::Err> {
        std::result::Result::Ok(Payload(x.to_string()))
    }
}

impl std::convert::From<Payload> for String {
    fn from(x: Payload) -> Self {
        x.0
    }
}

impl std::ops::Deref for Payload {
    type Target = String;
    fn deref(&self) -> &String {
        &self.0
    }
}

impl std::ops::DerefMut for Payload {
    fn deref_mut(&mut self) -> &mut String {
        &mut self.0
    }
}



/// The message to transmit
// Methods for converting between header::IntoHeaderValue<PushMessage> and hyper::header::HeaderValue

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<header::IntoHeaderValue<PushMessage>> for hyper::header::HeaderValue {
    type Error = String;

    fn try_from(hdr_value: header::IntoHeaderValue<PushMessage>) -> std::result::Result<Self, Self::Error> {
        let hdr_value = hdr_value.to_string();
        match hyper::header::HeaderValue::from_str(&hdr_value) {
             std::result::Result::Ok(value) => std::result::Result::Ok(value),
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Invalid header value for PushMessage - value: {} is invalid {}",
                     hdr_value, e))
        }
    }
}

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<hyper::header::HeaderValue> for header::IntoHeaderValue<PushMessage> {
    type Error = String;

    fn try_from(hdr_value: hyper::header::HeaderValue) -> std::result::Result<Self, Self::Error> {
        match hdr_value.to_str() {
             std::result::Result::Ok(value) => {
                    match <PushMessage as std::str::FromStr>::from_str(value) {
                        std::result::Result::Ok(value) => std::result::Result::Ok(header::IntoHeaderValue(value)),
                        std::result::Result::Err(err) => std::result::Result::Err(
                            format!("Unable to convert header value '{}' into PushMessage - {}",
                                value, err))
                    }
             },
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Unable to convert header: {:?} to string: {}",
                     hdr_value, e))
        }
    }
}


#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct PushMessage {
    /// Identifier for a resource
    #[serde(rename = "message-token")]
    pub message_token: String,

    /// the payload to transmit
    #[serde(rename = "payload")]
    pub payload: String,

    /// Identifier for a resource
    #[serde(rename = "subscription")]
    pub subscription: String,

}

impl PushMessage {
    pub fn new(message_token: String, payload: String, subscription: String, ) -> PushMessage {
        PushMessage {
            message_token: message_token,
            payload: payload,
            subscription: subscription,
        }
    }
}

/// Converts the PushMessage value to the Query Parameters representation (style=form, explode=false)
/// specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde serializer
impl std::string::ToString for PushMessage {
    fn to_string(&self) -> String {
        let mut params: Vec<String> = vec![];

        params.push("message-token".to_string());
        params.push(self.message_token.to_string());


        params.push("payload".to_string());
        params.push(self.payload.to_string());


        params.push("subscription".to_string());
        params.push(self.subscription.to_string());

        params.join(",").to_string()
    }
}

/// Converts Query Parameters representation (style=form, explode=false) to a PushMessage value
/// as specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde deserializer
impl std::str::FromStr for PushMessage {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        #[derive(Default)]
        // An intermediate representation of the struct to use for parsing.
        struct IntermediateRep {
            pub message_token: Vec<String>,
            pub payload: Vec<String>,
            pub subscription: Vec<String>,
        }

        let mut intermediate_rep = IntermediateRep::default();

        // Parse into intermediate representation
        let mut string_iter = s.split(',').into_iter();
        let mut key_result = string_iter.next();

        while key_result.is_some() {
            let val = match string_iter.next() {
                Some(x) => x,
                None => return std::result::Result::Err("Missing value while parsing PushMessage".to_string())
            };

            if let Some(key) = key_result {
                match key {
                    "message-token" => intermediate_rep.message_token.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    "payload" => intermediate_rep.payload.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    "subscription" => intermediate_rep.subscription.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    _ => return std::result::Result::Err("Unexpected key while parsing PushMessage".to_string())
                }
            }

            // Get the next key
            key_result = string_iter.next();
        }

        // Use the intermediate representation to return the struct
        std::result::Result::Ok(PushMessage {
            message_token: intermediate_rep.message_token.into_iter().next().ok_or("message-token missing in PushMessage".to_string())?,
            payload: intermediate_rep.payload.into_iter().next().ok_or("payload missing in PushMessage".to_string())?,
            subscription: intermediate_rep.subscription.into_iter().next().ok_or("subscription missing in PushMessage".to_string())?,
        })
    }
}



/// Rquest for push message delivery
// Methods for converting between header::IntoHeaderValue<PushRequest> and hyper::header::HeaderValue

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<header::IntoHeaderValue<PushRequest>> for hyper::header::HeaderValue {
    type Error = String;

    fn try_from(hdr_value: header::IntoHeaderValue<PushRequest>) -> std::result::Result<Self, Self::Error> {
        let hdr_value = hdr_value.to_string();
        match hyper::header::HeaderValue::from_str(&hdr_value) {
             std::result::Result::Ok(value) => std::result::Result::Ok(value),
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Invalid header value for PushRequest - value: {} is invalid {}",
                     hdr_value, e))
        }
    }
}

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<hyper::header::HeaderValue> for header::IntoHeaderValue<PushRequest> {
    type Error = String;

    fn try_from(hdr_value: hyper::header::HeaderValue) -> std::result::Result<Self, Self::Error> {
        match hdr_value.to_str() {
             std::result::Result::Ok(value) => {
                    match <PushRequest as std::str::FromStr>::from_str(value) {
                        std::result::Result::Ok(value) => std::result::Result::Ok(header::IntoHeaderValue(value)),
                        std::result::Result::Err(err) => std::result::Result::Err(
                            format!("Unable to convert header value '{}' into PushRequest - {}",
                                value, err))
                    }
             },
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Unable to convert header: {:?} to string: {}",
                     hdr_value, e))
        }
    }
}


#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct PushRequest {
    /// Time to live. For how long we should try delivering this message.
    #[serde(rename = "ttl")]
    pub ttl: usize,

    #[serde(rename = "urgency")]
    #[serde(skip_serializing_if="Option::is_none")]
    pub urgency: Option<models::Urgency>,

    #[serde(rename = "topic")]
    #[serde(skip_serializing_if="Option::is_none")]
    pub topic: Option<String>,

    /// the payload to transmit
    #[serde(rename = "payload")]
    pub payload: String,

}

impl PushRequest {
    pub fn new(ttl: usize, payload: String, ) -> PushRequest {
        PushRequest {
            ttl: ttl,
            urgency: None,
            topic: None,
            payload: payload,
        }
    }
}

/// Converts the PushRequest value to the Query Parameters representation (style=form, explode=false)
/// specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde serializer
impl std::string::ToString for PushRequest {
    fn to_string(&self) -> String {
        let mut params: Vec<String> = vec![];

        params.push("ttl".to_string());
        params.push(self.ttl.to_string());

        // Skipping urgency in query parameter serialization


        if let Some(ref topic) = self.topic {
            params.push("topic".to_string());
            params.push(topic.to_string());
        }


        params.push("payload".to_string());
        params.push(self.payload.to_string());

        params.join(",").to_string()
    }
}

/// Converts Query Parameters representation (style=form, explode=false) to a PushRequest value
/// as specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde deserializer
impl std::str::FromStr for PushRequest {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        #[derive(Default)]
        // An intermediate representation of the struct to use for parsing.
        struct IntermediateRep {
            pub ttl: Vec<usize>,
            pub urgency: Vec<models::Urgency>,
            pub topic: Vec<String>,
            pub payload: Vec<String>,
        }

        let mut intermediate_rep = IntermediateRep::default();

        // Parse into intermediate representation
        let mut string_iter = s.split(',').into_iter();
        let mut key_result = string_iter.next();

        while key_result.is_some() {
            let val = match string_iter.next() {
                Some(x) => x,
                None => return std::result::Result::Err("Missing value while parsing PushRequest".to_string())
            };

            if let Some(key) = key_result {
                match key {
                    "ttl" => intermediate_rep.ttl.push(usize::from_str(val).map_err(|x| format!("{}", x))?),
                    "urgency" => intermediate_rep.urgency.push(models::Urgency::from_str(val).map_err(|x| format!("{}", x))?),
                    "topic" => intermediate_rep.topic.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    "payload" => intermediate_rep.payload.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    _ => return std::result::Result::Err("Unexpected key while parsing PushRequest".to_string())
                }
            }

            // Get the next key
            key_result = string_iter.next();
        }

        // Use the intermediate representation to return the struct
        std::result::Result::Ok(PushRequest {
            ttl: intermediate_rep.ttl.into_iter().next().ok_or("ttl missing in PushRequest".to_string())?,
            urgency: intermediate_rep.urgency.into_iter().next(),
            topic: intermediate_rep.topic.into_iter().next(),
            payload: intermediate_rep.payload.into_iter().next().ok_or("payload missing in PushRequest".to_string())?,
        })
    }
}



// Methods for converting between header::IntoHeaderValue<Subscription> and hyper::header::HeaderValue

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<header::IntoHeaderValue<Subscription>> for hyper::header::HeaderValue {
    type Error = String;

    fn try_from(hdr_value: header::IntoHeaderValue<Subscription>) -> std::result::Result<Self, Self::Error> {
        let hdr_value = hdr_value.to_string();
        match hyper::header::HeaderValue::from_str(&hdr_value) {
             std::result::Result::Ok(value) => std::result::Result::Ok(value),
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Invalid header value for Subscription - value: {} is invalid {}",
                     hdr_value, e))
        }
    }
}

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<hyper::header::HeaderValue> for header::IntoHeaderValue<Subscription> {
    type Error = String;

    fn try_from(hdr_value: hyper::header::HeaderValue) -> std::result::Result<Self, Self::Error> {
        match hdr_value.to_str() {
             std::result::Result::Ok(value) => {
                    match <Subscription as std::str::FromStr>::from_str(value) {
                        std::result::Result::Ok(value) => std::result::Result::Ok(header::IntoHeaderValue(value)),
                        std::result::Result::Err(err) => std::result::Result::Err(
                            format!("Unable to convert header value '{}' into Subscription - {}",
                                value, err))
                    }
             },
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Unable to convert header: {:?} to string: {}",
                     hdr_value, e))
        }
    }
}


#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct Subscription {
    /// Identifier for a resource
    #[serde(rename = "subscription-token")]
    pub subscription_token: String,

    /// Identifier for a resource
    #[serde(rename = "push-token")]
    pub push_token: String,

    #[serde(rename = "subscription-set")]
    pub subscription_set: models::SubscriptionSet,

}

impl Subscription {
    pub fn new(subscription_token: String, push_token: String, subscription_set: models::SubscriptionSet, ) -> Subscription {
        Subscription {
            subscription_token: subscription_token,
            push_token: push_token,
            subscription_set: subscription_set,
        }
    }
}

/// Converts the Subscription value to the Query Parameters representation (style=form, explode=false)
/// specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde serializer
impl std::string::ToString for Subscription {
    fn to_string(&self) -> String {
        let mut params: Vec<String> = vec![];

        params.push("subscription-token".to_string());
        params.push(self.subscription_token.to_string());


        params.push("push-token".to_string());
        params.push(self.push_token.to_string());

        // Skipping subscription-set in query parameter serialization

        params.join(",").to_string()
    }
}

/// Converts Query Parameters representation (style=form, explode=false) to a Subscription value
/// as specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde deserializer
impl std::str::FromStr for Subscription {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        #[derive(Default)]
        // An intermediate representation of the struct to use for parsing.
        struct IntermediateRep {
            pub subscription_token: Vec<String>,
            pub push_token: Vec<String>,
            pub subscription_set: Vec<models::SubscriptionSet>,
        }

        let mut intermediate_rep = IntermediateRep::default();

        // Parse into intermediate representation
        let mut string_iter = s.split(',').into_iter();
        let mut key_result = string_iter.next();

        while key_result.is_some() {
            let val = match string_iter.next() {
                Some(x) => x,
                None => return std::result::Result::Err("Missing value while parsing Subscription".to_string())
            };

            if let Some(key) = key_result {
                match key {
                    "subscription-token" => intermediate_rep.subscription_token.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    "push-token" => intermediate_rep.push_token.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    "subscription-set" => intermediate_rep.subscription_set.push(models::SubscriptionSet::from_str(val).map_err(|x| format!("{}", x))?),
                    _ => return std::result::Result::Err("Unexpected key while parsing Subscription".to_string())
                }
            }

            // Get the next key
            key_result = string_iter.next();
        }

        // Use the intermediate representation to return the struct
        std::result::Result::Ok(Subscription {
            subscription_token: intermediate_rep.subscription_token.into_iter().next().ok_or("subscription-token missing in Subscription".to_string())?,
            push_token: intermediate_rep.push_token.into_iter().next().ok_or("push-token missing in Subscription".to_string())?,
            subscription_set: intermediate_rep.subscription_set.into_iter().next().ok_or("subscription-set missing in Subscription".to_string())?,
        })
    }
}



// Methods for converting between header::IntoHeaderValue<SubscriptionSet> and hyper::header::HeaderValue

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<header::IntoHeaderValue<SubscriptionSet>> for hyper::header::HeaderValue {
    type Error = String;

    fn try_from(hdr_value: header::IntoHeaderValue<SubscriptionSet>) -> std::result::Result<Self, Self::Error> {
        let hdr_value = hdr_value.to_string();
        match hyper::header::HeaderValue::from_str(&hdr_value) {
             std::result::Result::Ok(value) => std::result::Result::Ok(value),
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Invalid header value for SubscriptionSet - value: {} is invalid {}",
                     hdr_value, e))
        }
    }
}

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<hyper::header::HeaderValue> for header::IntoHeaderValue<SubscriptionSet> {
    type Error = String;

    fn try_from(hdr_value: hyper::header::HeaderValue) -> std::result::Result<Self, Self::Error> {
        match hdr_value.to_str() {
             std::result::Result::Ok(value) => {
                    match <SubscriptionSet as std::str::FromStr>::from_str(value) {
                        std::result::Result::Ok(value) => std::result::Result::Ok(header::IntoHeaderValue(value)),
                        std::result::Result::Err(err) => std::result::Result::Err(
                            format!("Unable to convert header value '{}' into SubscriptionSet - {}",
                                value, err))
                    }
             },
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Unable to convert header: {:?} to string: {}",
                     hdr_value, e))
        }
    }
}


#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct SubscriptionSet {
    /// Identifier for a resource
    #[serde(rename = "subscription-set-token")]
    pub subscription_set_token: String,

}

impl SubscriptionSet {
    pub fn new(subscription_set_token: String, ) -> SubscriptionSet {
        SubscriptionSet {
            subscription_set_token: subscription_set_token,
        }
    }
}

/// Converts the SubscriptionSet value to the Query Parameters representation (style=form, explode=false)
/// specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde serializer
impl std::string::ToString for SubscriptionSet {
    fn to_string(&self) -> String {
        let mut params: Vec<String> = vec![];

        params.push("subscription-set-token".to_string());
        params.push(self.subscription_set_token.to_string());

        params.join(",").to_string()
    }
}

/// Converts Query Parameters representation (style=form, explode=false) to a SubscriptionSet value
/// as specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde deserializer
impl std::str::FromStr for SubscriptionSet {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        #[derive(Default)]
        // An intermediate representation of the struct to use for parsing.
        struct IntermediateRep {
            pub subscription_set_token: Vec<String>,
        }

        let mut intermediate_rep = IntermediateRep::default();

        // Parse into intermediate representation
        let mut string_iter = s.split(',').into_iter();
        let mut key_result = string_iter.next();

        while key_result.is_some() {
            let val = match string_iter.next() {
                Some(x) => x,
                None => return std::result::Result::Err("Missing value while parsing SubscriptionSet".to_string())
            };

            if let Some(key) = key_result {
                match key {
                    "subscription-set-token" => intermediate_rep.subscription_set_token.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    _ => return std::result::Result::Err("Unexpected key while parsing SubscriptionSet".to_string())
                }
            }

            // Get the next key
            key_result = string_iter.next();
        }

        // Use the intermediate representation to return the struct
        std::result::Result::Ok(SubscriptionSet {
            subscription_set_token: intermediate_rep.subscription_set_token.into_iter().next().ok_or("subscription-set-token missing in SubscriptionSet".to_string())?,
        })
    }
}



/// Identifier for a resource
#[derive(Debug, Clone, PartialEq, PartialOrd, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct Token(String);

impl std::convert::From<String> for Token {
    fn from(x: String) -> Self {
        Token(x)
    }
}

impl std::string::ToString for Token {
    fn to_string(&self) -> String {
       self.0.to_string()
    }
}

impl std::str::FromStr for Token {
    type Err = std::string::ParseError;
    fn from_str(x: &str) -> std::result::Result<Self, Self::Err> {
        std::result::Result::Ok(Token(x.to_string()))
    }
}

impl std::convert::From<Token> for String {
    fn from(x: Token) -> Self {
        x.0
    }
}

impl std::ops::Deref for Token {
    type Target = String;
    fn deref(&self) -> &String {
        &self.0
    }
}

impl std::ops::DerefMut for Token {
    fn deref_mut(&mut self) -> &mut String {
        &mut self.0
    }
}



/// Urgency of a push message
/// Enumeration of values.
/// Since this enum's variants do not hold data, we can easily define them them as `#[repr(C)]`
/// which helps with FFI.
#[allow(non_camel_case_types)]
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk_enum_derive::LabelledGenericEnum))]
pub enum Urgency { 
    #[serde(rename = "very-low")]
    VERY_LOW,
    #[serde(rename = "low")]
    LOW,
    #[serde(rename = "normal")]
    NORMAL,
    #[serde(rename = "high")]
    HIGH,
}

impl std::fmt::Display for Urgency {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self { 
            Urgency::VERY_LOW => write!(f, "{}", "very-low"),
            Urgency::LOW => write!(f, "{}", "low"),
            Urgency::NORMAL => write!(f, "{}", "normal"),
            Urgency::HIGH => write!(f, "{}", "high"),
        }
    }
}

impl std::str::FromStr for Urgency {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "very-low" => std::result::Result::Ok(Urgency::VERY_LOW),
            "low" => std::result::Result::Ok(Urgency::LOW),
            "normal" => std::result::Result::Ok(Urgency::NORMAL),
            "high" => std::result::Result::Ok(Urgency::HIGH),
            _ => std::result::Result::Err(format!("Value not valid: {}", s)),
        }
    }
}


/// Version of the API
// Methods for converting between header::IntoHeaderValue<Version> and hyper::header::HeaderValue

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<header::IntoHeaderValue<Version>> for hyper::header::HeaderValue {
    type Error = String;

    fn try_from(hdr_value: header::IntoHeaderValue<Version>) -> std::result::Result<Self, Self::Error> {
        let hdr_value = hdr_value.to_string();
        match hyper::header::HeaderValue::from_str(&hdr_value) {
             std::result::Result::Ok(value) => std::result::Result::Ok(value),
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Invalid header value for Version - value: {} is invalid {}",
                     hdr_value, e))
        }
    }
}

#[cfg(any(feature = "client", feature = "server"))]
impl std::convert::TryFrom<hyper::header::HeaderValue> for header::IntoHeaderValue<Version> {
    type Error = String;

    fn try_from(hdr_value: hyper::header::HeaderValue) -> std::result::Result<Self, Self::Error> {
        match hdr_value.to_str() {
             std::result::Result::Ok(value) => {
                    match <Version as std::str::FromStr>::from_str(value) {
                        std::result::Result::Ok(value) => std::result::Result::Ok(header::IntoHeaderValue(value)),
                        std::result::Result::Err(err) => std::result::Result::Err(
                            format!("Unable to convert header value '{}' into Version - {}",
                                value, err))
                    }
             },
             std::result::Result::Err(e) => std::result::Result::Err(
                 format!("Unable to convert header: {:?} to string: {}",
                     hdr_value, e))
        }
    }
}


#[derive(Debug, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
#[cfg_attr(feature = "conversion", derive(frunk::LabelledGeneric))]
pub struct Version {
    #[serde(rename = "version")]
    pub version: String,

}

impl Version {
    pub fn new(version: String, ) -> Version {
        Version {
            version: version,
        }
    }
}

/// Converts the Version value to the Query Parameters representation (style=form, explode=false)
/// specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde serializer
impl std::string::ToString for Version {
    fn to_string(&self) -> String {
        let mut params: Vec<String> = vec![];

        params.push("version".to_string());
        params.push(self.version.to_string());

        params.join(",").to_string()
    }
}

/// Converts Query Parameters representation (style=form, explode=false) to a Version value
/// as specified in https://swagger.io/docs/specification/serialization/
/// Should be implemented in a serde deserializer
impl std::str::FromStr for Version {
    type Err = String;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        #[derive(Default)]
        // An intermediate representation of the struct to use for parsing.
        struct IntermediateRep {
            pub version: Vec<String>,
        }

        let mut intermediate_rep = IntermediateRep::default();

        // Parse into intermediate representation
        let mut string_iter = s.split(',').into_iter();
        let mut key_result = string_iter.next();

        while key_result.is_some() {
            let val = match string_iter.next() {
                Some(x) => x,
                None => return std::result::Result::Err("Missing value while parsing Version".to_string())
            };

            if let Some(key) = key_result {
                match key {
                    "version" => intermediate_rep.version.push(String::from_str(val).map_err(|x| format!("{}", x))?),
                    _ => return std::result::Result::Err("Unexpected key while parsing Version".to_string())
                }
            }

            // Get the next key
            key_result = string_iter.next();
        }

        // Use the intermediate representation to return the struct
        std::result::Result::Ok(Version {
            version: intermediate_rep.version.into_iter().next().ok_or("version missing in Version".to_string())?,
        })
    }
}



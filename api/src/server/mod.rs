use futures::{future, future::BoxFuture, Stream, stream, future::FutureExt, stream::TryStreamExt};
use hyper::{Request, Response, StatusCode, Body, HeaderMap};
use hyper::header::{HeaderName, HeaderValue, CONTENT_TYPE};
use log::warn;
#[allow(unused_imports)]
use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::future::Future;
use std::marker::PhantomData;
use std::task::{Context, Poll};
use swagger::{ApiError, BodyExt, Has, RequestParser, XSpanIdString};
pub use swagger::auth::Authorization;
use swagger::auth::Scopes;
use url::form_urlencoded;

#[allow(unused_imports)]
use crate::models;
use crate::header;

pub use crate::context;

type ServiceFuture = BoxFuture<'static, Result<Response<Body>, crate::ServiceError>>;

use crate::{Api,
     MessageTokenDeleteResponse,
     PushTokenPostResponse,
     SubscribePostResponse,
     SubscriptionSetTokenDeleteResponse,
     SubscriptionSetTokenGetResponse,
     SubscriptionTokenDeleteResponse,
     SubscriptionTokenGetResponse,
     VersionGetResponse
};

mod paths {
    use lazy_static::lazy_static;

    lazy_static! {
        pub static ref GLOBAL_REGEX_SET: regex::RegexSet = regex::RegexSet::new(vec![
            r"^/message/(?P<token>[^/?#]*)$",
            r"^/push/(?P<token>[^/?#]*)$",
            r"^/subscribe$",
            r"^/subscription-set/(?P<token>[^/?#]*)$",
            r"^/subscription/(?P<token>[^/?#]*)$",
            r"^/version$"
        ])
        .expect("Unable to create global regex set");
    }
    pub(crate) static ID_MESSAGE_TOKEN: usize = 0;
    lazy_static! {
        pub static ref REGEX_MESSAGE_TOKEN: regex::Regex =
            regex::Regex::new(r"^/message/(?P<token>[^/?#]*)$")
                .expect("Unable to create regex for MESSAGE_TOKEN");
    }
    pub(crate) static ID_PUSH_TOKEN: usize = 1;
    lazy_static! {
        pub static ref REGEX_PUSH_TOKEN: regex::Regex =
            regex::Regex::new(r"^/push/(?P<token>[^/?#]*)$")
                .expect("Unable to create regex for PUSH_TOKEN");
    }
    pub(crate) static ID_SUBSCRIBE: usize = 2;
    pub(crate) static ID_SUBSCRIPTION_SET_TOKEN: usize = 3;
    lazy_static! {
        pub static ref REGEX_SUBSCRIPTION_SET_TOKEN: regex::Regex =
            regex::Regex::new(r"^/subscription-set/(?P<token>[^/?#]*)$")
                .expect("Unable to create regex for SUBSCRIPTION_SET_TOKEN");
    }
    pub(crate) static ID_SUBSCRIPTION_TOKEN: usize = 4;
    lazy_static! {
        pub static ref REGEX_SUBSCRIPTION_TOKEN: regex::Regex =
            regex::Regex::new(r"^/subscription/(?P<token>[^/?#]*)$")
                .expect("Unable to create regex for SUBSCRIPTION_TOKEN");
    }
    pub(crate) static ID_VERSION: usize = 5;
}

pub struct MakeService<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    api_impl: T,
    marker: PhantomData<C>,
}

impl<T, C> MakeService<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    pub fn new(api_impl: T) -> Self {
        MakeService {
            api_impl,
            marker: PhantomData
        }
    }
}

impl<T, C, Target> hyper::service::Service<Target> for MakeService<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    type Response = Service<T, C>;
    type Error = crate::ServiceError;
    type Future = future::Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, target: Target) -> Self::Future {
        futures::future::ok(Service::new(
            self.api_impl.clone(),
        ))
    }
}

fn method_not_allowed() -> Result<Response<Body>, crate::ServiceError> {
    Ok(
        Response::builder().status(StatusCode::METHOD_NOT_ALLOWED)
            .body(Body::empty())
            .expect("Unable to create Method Not Allowed response")
    )
}

pub struct Service<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    api_impl: T,
    marker: PhantomData<C>,
}

impl<T, C> Service<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    pub fn new(api_impl: T) -> Self {
        Service {
            api_impl: api_impl,
            marker: PhantomData
        }
    }
}

impl<T, C> Clone for Service<T, C> where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    fn clone(&self) -> Self {
        Service {
            api_impl: self.api_impl.clone(),
            marker: self.marker.clone(),
        }
    }
}

impl<T, C> hyper::service::Service<(Request<Body>, C)> for Service<T, C> where
    T: Api<C> + Clone + Send + Sync + 'static,
    C: Has<XSpanIdString>  + Send + Sync + 'static
{
    type Response = Response<Body>;
    type Error = crate::ServiceError;
    type Future = ServiceFuture;

    fn poll_ready(&mut self, cx: &mut Context) -> Poll<Result<(), Self::Error>> {
        self.api_impl.poll_ready(cx)
    }

    fn call(&mut self, req: (Request<Body>, C)) -> Self::Future { async fn run<T, C>(mut api_impl: T, req: (Request<Body>, C)) -> Result<Response<Body>, crate::ServiceError> where
        T: Api<C> + Clone + Send + 'static,
        C: Has<XSpanIdString>  + Send + Sync + 'static
    {
        let (request, context) = req;
        let (parts, body) = request.into_parts();
        let (method, uri, headers) = (parts.method, parts.uri, parts.headers);
        let path = paths::GLOBAL_REGEX_SET.matches(uri.path());

        match &method {

            // MessageTokenDelete - DELETE /message/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_MESSAGE_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_MESSAGE_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE MESSAGE_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_MESSAGE_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                                let result = api_impl.message_token_delete(
                                            param_token,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                MessageTokenDeleteResponse::Deleted
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                },
                                                MessageTokenDeleteResponse::InvalidMessageToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(400).expect("Unable to turn 400 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            // PushTokenPost - POST /push/{token}
            &hyper::Method::POST if path.matched(paths::ID_PUSH_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_PUSH_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE PUSH_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_PUSH_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                // Body parameters (note that non-required body parameters will ignore garbage
                // values, rather than causing a 400 response). Produce warning header and logs for
                // any unused fields.
                let result = body.to_raw().await;
                match result {
                            Ok(body) => {
                                let mut unused_elements = Vec::new();
                                let param_push_request: Option<models::PushRequest> = if !body.is_empty() {
                                    let deserializer = &mut serde_json::Deserializer::from_slice(&*body);
                                    match serde_ignored::deserialize(deserializer, |path| {
                                            warn!("Ignoring unknown field in body: {}", path);
                                            unused_elements.push(path.to_string());
                                    }) {
                                        Ok(param_push_request) => param_push_request,
                                        Err(e) => return Ok(Response::builder()
                                                        .status(StatusCode::BAD_REQUEST)
                                                        .body(Body::from(format!("Couldn't parse body parameter PushRequest - doesn't match schema: {}", e)))
                                                        .expect("Unable to create Bad Request response for invalid body parameter PushRequest due to schema")),
                                    }
                                } else {
                                    None
                                };
                                let param_push_request = match param_push_request {
                                    Some(param_push_request) => param_push_request,
                                    None => return Ok(Response::builder()
                                                        .status(StatusCode::BAD_REQUEST)
                                                        .body(Body::from("Missing required body parameter PushRequest"))
                                                        .expect("Unable to create Bad Request response for missing body parameter PushRequest")),
                                };

                                let result = api_impl.push_token_post(
                                            param_token,
                                            param_push_request,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        if !unused_elements.is_empty() {
                                            response.headers_mut().insert(
                                                HeaderName::from_static("warning"),
                                                HeaderValue::from_str(format!("Ignoring unknown fields in body: {:?}", unused_elements).as_str())
                                                    .expect("Unable to create Warning header value"));
                                        }

                                        match result {
                                            Ok(rsp) => match rsp {
                                                PushTokenPostResponse::Created
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(201).expect("Unable to turn 201 into a StatusCode");
                                                },
                                                PushTokenPostResponse::InvalidPushToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(404).expect("Unable to turn 404 into a StatusCode");
                                                },
                                                PushTokenPostResponse::PayloadTooLarge
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(413).expect("Unable to turn 413 into a StatusCode");
                                                },
                                                PushTokenPostResponse::TooManyRequests
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(429).expect("Unable to turn 429 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
                            },
                            Err(e) => Ok(Response::builder()
                                                .status(StatusCode::BAD_REQUEST)
                                                .body(Body::from(format!("Couldn't read body parameter PushRequest: {}", e)))
                                                .expect("Unable to create Bad Request response due to unable to read body parameter PushRequest")),
                        }
            },

            // SubscribePost - POST /subscribe
            &hyper::Method::POST if path.matched(paths::ID_SUBSCRIBE) => {
                // Body parameters (note that non-required body parameters will ignore garbage
                // values, rather than causing a 400 response). Produce warning header and logs for
                // any unused fields.
                let result = body.to_raw().await;
                match result {
                            Ok(body) => {
                                let mut unused_elements = Vec::new();
                                let param_subscription_set: Option<models::SubscriptionSet> = if !body.is_empty() {
                                    let deserializer = &mut serde_json::Deserializer::from_slice(&*body);
                                    match serde_ignored::deserialize(deserializer, |path| {
                                            warn!("Ignoring unknown field in body: {}", path);
                                            unused_elements.push(path.to_string());
                                    }) {
                                        Ok(param_subscription_set) => param_subscription_set,
                                        Err(_) => None,
                                    }
                                } else {
                                    None
                                };

                                let result = api_impl.subscribe_post(
                                            param_subscription_set,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        if !unused_elements.is_empty() {
                                            response.headers_mut().insert(
                                                HeaderName::from_static("warning"),
                                                HeaderValue::from_str(format!("Ignoring unknown fields in body: {:?}", unused_elements).as_str())
                                                    .expect("Unable to create Warning header value"));
                                        }

                                        match result {
                                            Ok(rsp) => match rsp {
                                                SubscribePostResponse::Created
                                                    (body)
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(201).expect("Unable to turn 201 into a StatusCode");
                                                    response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for SUBSCRIBE_POST_CREATED"));
                                                    let body = serde_json::to_string(&body).expect("impossible to fail to serialize");
                                                    *response.body_mut() = Body::from(body);
                                                },
                                                SubscribePostResponse::InvalidSubscriptionSet
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(400).expect("Unable to turn 400 into a StatusCode");
                                                },
                                                SubscribePostResponse::TooManyRequests
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(429).expect("Unable to turn 429 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
                            },
                            Err(e) => Ok(Response::builder()
                                                .status(StatusCode::BAD_REQUEST)
                                                .body(Body::from(format!("Couldn't read body parameter SubscriptionSet: {}", e)))
                                                .expect("Unable to create Bad Request response due to unable to read body parameter SubscriptionSet")),
                        }
            },

            // SubscriptionSetTokenDelete - DELETE /subscription-set/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_SUBSCRIPTION_SET_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_SUBSCRIPTION_SET_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE SUBSCRIPTION_SET_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_SUBSCRIPTION_SET_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                                let result = api_impl.subscription_set_token_delete(
                                            param_token,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                SubscriptionSetTokenDeleteResponse::Deleted
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                },
                                                SubscriptionSetTokenDeleteResponse::InvalidSubscriptionSetToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(404).expect("Unable to turn 404 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            // SubscriptionSetTokenGet - GET /subscription-set/{token}
            &hyper::Method::GET if path.matched(paths::ID_SUBSCRIPTION_SET_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_SUBSCRIPTION_SET_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE SUBSCRIPTION_SET_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_SUBSCRIPTION_SET_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                                let result = api_impl.subscription_set_token_get(
                                            param_token,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                SubscriptionSetTokenGetResponse::SubscriptionSetMessages
                                                    (body)
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                    response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for SUBSCRIPTION_SET_TOKEN_GET_SUBSCRIPTION_SET_MESSAGES"));
                                                    let body = serde_json::to_string(&body).expect("impossible to fail to serialize");
                                                    *response.body_mut() = Body::from(body);
                                                },
                                                SubscriptionSetTokenGetResponse::InvalidSubscriptionSetToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(404).expect("Unable to turn 404 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            // SubscriptionTokenDelete - DELETE /subscription/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_SUBSCRIPTION_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_SUBSCRIPTION_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE SUBSCRIPTION_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_SUBSCRIPTION_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                                let result = api_impl.subscription_token_delete(
                                            param_token,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                SubscriptionTokenDeleteResponse::Deleted
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                },
                                                SubscriptionTokenDeleteResponse::InvalidSubscriptionToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(404).expect("Unable to turn 404 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            // SubscriptionTokenGet - GET /subscription/{token}
            &hyper::Method::GET if path.matched(paths::ID_SUBSCRIPTION_TOKEN) => {
                // Path parameters
                let path: &str = &uri.path().to_string();
                let path_params =
                    paths::REGEX_SUBSCRIPTION_TOKEN
                    .captures(&path)
                    .unwrap_or_else(||
                        panic!("Path {} matched RE SUBSCRIPTION_TOKEN in set but failed match against \"{}\"", path, paths::REGEX_SUBSCRIPTION_TOKEN.as_str())
                    );

                let param_token = match percent_encoding::percent_decode(path_params["token"].as_bytes()).decode_utf8() {
                    Ok(param_token) => match param_token.parse::<String>() {
                        Ok(param_token) => param_token,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter token: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["token"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                                let result = api_impl.subscription_token_get(
                                            param_token,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                SubscriptionTokenGetResponse::SubscriptionMessages
                                                    (body)
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                    response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for SUBSCRIPTION_TOKEN_GET_SUBSCRIPTION_MESSAGES"));
                                                    let body = serde_json::to_string(&body).expect("impossible to fail to serialize");
                                                    *response.body_mut() = Body::from(body);
                                                },
                                                SubscriptionTokenGetResponse::InvalidSubscriptionToken
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(404).expect("Unable to turn 404 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            // VersionGet - GET /version
            &hyper::Method::GET if path.matched(paths::ID_VERSION) => {
                                let result = api_impl.version_get(
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                VersionGetResponse::ServerApiVersion
                                                    (body)
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                    response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for VERSION_GET_SERVER_API_VERSION"));
                                                    let body = serde_json::to_string(&body).expect("impossible to fail to serialize");
                                                    *response.body_mut() = Body::from(body);
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
            },

            _ if path.matched(paths::ID_MESSAGE_TOKEN) => method_not_allowed(),
            _ if path.matched(paths::ID_PUSH_TOKEN) => method_not_allowed(),
            _ if path.matched(paths::ID_SUBSCRIBE) => method_not_allowed(),
            _ if path.matched(paths::ID_SUBSCRIPTION_SET_TOKEN) => method_not_allowed(),
            _ if path.matched(paths::ID_SUBSCRIPTION_TOKEN) => method_not_allowed(),
            _ if path.matched(paths::ID_VERSION) => method_not_allowed(),
            _ => Ok(Response::builder().status(StatusCode::NOT_FOUND)
                    .body(Body::empty())
                    .expect("Unable to create Not Found response"))
        }
    } Box::pin(run(self.api_impl.clone(), req)) }
}

/// Request parser for `Api`.
pub struct ApiRequestParser;
impl<T> RequestParser<T> for ApiRequestParser {
    fn parse_operation_id(request: &Request<T>) -> Result<&'static str, ()> {
        let path = paths::GLOBAL_REGEX_SET.matches(request.uri().path());
        match request.method() {
            // MessageTokenDelete - DELETE /message/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_MESSAGE_TOKEN) => Ok("MessageTokenDelete"),
            // PushTokenPost - POST /push/{token}
            &hyper::Method::POST if path.matched(paths::ID_PUSH_TOKEN) => Ok("PushTokenPost"),
            // SubscribePost - POST /subscribe
            &hyper::Method::POST if path.matched(paths::ID_SUBSCRIBE) => Ok("SubscribePost"),
            // SubscriptionSetTokenDelete - DELETE /subscription-set/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_SUBSCRIPTION_SET_TOKEN) => Ok("SubscriptionSetTokenDelete"),
            // SubscriptionSetTokenGet - GET /subscription-set/{token}
            &hyper::Method::GET if path.matched(paths::ID_SUBSCRIPTION_SET_TOKEN) => Ok("SubscriptionSetTokenGet"),
            // SubscriptionTokenDelete - DELETE /subscription/{token}
            &hyper::Method::DELETE if path.matched(paths::ID_SUBSCRIPTION_TOKEN) => Ok("SubscriptionTokenDelete"),
            // SubscriptionTokenGet - GET /subscription/{token}
            &hyper::Method::GET if path.matched(paths::ID_SUBSCRIPTION_TOKEN) => Ok("SubscriptionTokenGet"),
            // VersionGet - GET /version
            &hyper::Method::GET if path.matched(paths::ID_VERSION) => Ok("VersionGet"),
            _ => Err(()),
        }
    }
}

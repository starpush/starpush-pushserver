# PushRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ttl** | **usize** | Time to live. For how long we should try delivering this message. | 
**urgency** | [***models::Urgency**](Urgency.md) |  | [optional] [default to None]
**topic** | **String** |  | [optional] [default to None]
**payload** | **String** | the payload to transmit | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



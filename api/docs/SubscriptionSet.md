# SubscriptionSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_set_token** | **String** | Identifier for a resource | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Subscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_token** | **String** | Identifier for a resource | 
**push_token** | **String** | Identifier for a resource | 
**subscription_set** | [***models::SubscriptionSet**](SubscriptionSet.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



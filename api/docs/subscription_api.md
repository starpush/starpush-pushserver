# subscription_api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
****](subscription_api.md#) | **POST** /subscribe | create a new push subscription
****](subscription_api.md#) | **DELETE** /subscription-set/{token} | Delete the subscription set and all related subscriptions.
****](subscription_api.md#) | **GET** /subscription-set/{token} | Request messages belonging to this subscription set.
****](subscription_api.md#) | **DELETE** /subscription/{token} | Delete the subscription.
****](subscription_api.md#) | **GET** /subscription/{token} | Request messages belonging to this subscription.


# ****
> models::Subscription (optional)
create a new push subscription

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscription_set** | [**SubscriptionSet**](SubscriptionSet.md)|  | 

### Return type

[**models::Subscription**](Subscription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, 

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# ****
> (token)
Delete the subscription set and all related subscriptions.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token for the subscription set. | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# ****
> Vec<models::PushMessage> (token)
Request messages belonging to this subscription set.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token for the subscription set. | 

### Return type

[**Vec<models::PushMessage>**](PushMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, 

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# ****
> (token)
Delete the subscription.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token for the subscription. | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# ****
> Vec<models::PushMessage> (token)
Request messages belonging to this subscription.

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token for the subscription. | 

### Return type

[**Vec<models::PushMessage>**](PushMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


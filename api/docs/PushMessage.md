# PushMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message_token** | **String** | Identifier for a resource | 
**payload** | **String** | the payload to transmit | 
**subscription** | **String** | Identifier for a resource | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



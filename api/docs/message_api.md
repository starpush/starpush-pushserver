# message_api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
****](message_api.md#) | **DELETE** /message/{token} | Delete a message that was previously sent
****](message_api.md#) | **POST** /push/{token} | Request push message delivery


# ****
> (token)
Delete a message that was previously sent

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token for the push message. | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# ****
> (token, push_request)
Request push message delivery

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **token** | **String**| The token to access the push resource. | 
  **push_request** | [**PushRequest**](PushRequest.md)| the message | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


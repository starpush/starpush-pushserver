//! Main library entry point for openapi_client implementation.

#![allow(unused_imports)]

use async_trait::async_trait;
use futures::{future, Stream, StreamExt, TryFutureExt, TryStreamExt};
use hyper::server::conn::Http;
use hyper::service::Service;
use log::info;
use openssl::ssl::SslAcceptorBuilder;
use std::future::Future;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll};
use swagger::{Has, XSpanIdString};
use swagger::auth::MakeAllowAllAuthenticator;
use swagger::EmptyContext;
use tokio::net::TcpListener;

#[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use openapi_client::models;

#[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
/// Builds an SSL implementation for Simple HTTPS from some hard-coded file names
pub async fn create(addr: &str, https: bool) {
    let addr = addr.parse().expect("Failed to parse bind address");

    let server = Server::new();

    let service = MakeService::new(server);

    let service = MakeAllowAllAuthenticator::new(service, "cosmo");

    let mut service =
        openapi_client::server::context::MakeAddContext::<_, EmptyContext>::new(
            service
        );

    if https {
        #[cfg(any(target_os = "macos", target_os = "windows", target_os = "ios"))]
        {
            unimplemented!("SSL is not implemented for the examples on MacOS, Windows or iOS");
        }

        #[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
        {
            let mut ssl = SslAcceptor::mozilla_intermediate_v5(SslMethod::tls()).expect("Failed to create SSL Acceptor");

            // Server authentication
            ssl.set_private_key_file("examples/server-key.pem", SslFiletype::PEM).expect("Failed to set private key");
            ssl.set_certificate_chain_file("examples/server-chain.pem").expect("Failed to set cerificate chain");
            ssl.check_private_key().expect("Failed to check private key");

            let tls_acceptor = Arc::new(ssl.build());
            let mut tcp_listener = TcpListener::bind(&addr).await.unwrap();
            let mut incoming = tcp_listener.incoming();

            while let (Some(tcp), rest) = incoming.into_future().await {
                if let Ok(tcp) = tcp {
                    let addr = tcp.peer_addr().expect("Unable to get remote address");
                    let service = service.call(addr);
                    let tls_acceptor = Arc::clone(&tls_acceptor);

                    tokio::spawn(async move {
                        let tls = tokio_openssl::accept(&*tls_acceptor, tcp).await.map_err(|_| ())?;

                        let service = service.await.map_err(|_| ())?;

                        Http::new().serve_connection(tls, service).await.map_err(|_| ())
                    });
                }

                incoming = rest;
            }
        }
    } else {
        // Using HTTP
        hyper::server::Server::bind(&addr).serve(service).await.unwrap()
    }
}

#[derive(Copy, Clone)]
pub struct Server<C> {
    marker: PhantomData<C>,
}

impl<C> Server<C> {
    pub fn new() -> Self {
        Server{marker: PhantomData}
    }
}


use openapi_client::{
    Api,
    MessageTokenDeleteResponse,
    PushTokenPostResponse,
    SubscribePostResponse,
    SubscriptionSetTokenDeleteResponse,
    SubscriptionSetTokenGetResponse,
    SubscriptionTokenDeleteResponse,
    SubscriptionTokenGetResponse,
    VersionGetResponse,
};
use openapi_client::server::MakeService;
use std::error::Error;
use swagger::ApiError;

#[async_trait]
impl<C> Api<C> for Server<C> where C: Has<XSpanIdString> + Send + Sync
{
    /// Delete a message that was previously sent
    async fn message_token_delete(
        &self,
        token: String,
        context: &C) -> Result<MessageTokenDeleteResponse, ApiError>
    {
        let context = context.clone();
        info!("message_token_delete(\"{}\") - X-Span-ID: {:?}", token, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Request push message delivery
    async fn push_token_post(
        &self,
        token: String,
        push_request: models::PushRequest,
        context: &C) -> Result<PushTokenPostResponse, ApiError>
    {
        let context = context.clone();
        info!("push_token_post(\"{}\", {:?}) - X-Span-ID: {:?}", token, push_request, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// create a new push subscription
    async fn subscribe_post(
        &self,
        subscription_set: Option<models::SubscriptionSet>,
        context: &C) -> Result<SubscribePostResponse, ApiError>
    {
        let context = context.clone();
        info!("subscribe_post({:?}) - X-Span-ID: {:?}", subscription_set, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Delete the subscription set and all related subscriptions.
    async fn subscription_set_token_delete(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionSetTokenDeleteResponse, ApiError>
    {
        let context = context.clone();
        info!("subscription_set_token_delete(\"{}\") - X-Span-ID: {:?}", token, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Request messages belonging to this subscription set.
    async fn subscription_set_token_get(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionSetTokenGetResponse, ApiError>
    {
        let context = context.clone();
        info!("subscription_set_token_get(\"{}\") - X-Span-ID: {:?}", token, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Delete the subscription.
    async fn subscription_token_delete(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionTokenDeleteResponse, ApiError>
    {
        let context = context.clone();
        info!("subscription_token_delete(\"{}\") - X-Span-ID: {:?}", token, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Request messages belonging to this subscription.
    async fn subscription_token_get(
        &self,
        token: String,
        context: &C) -> Result<SubscriptionTokenGetResponse, ApiError>
    {
        let context = context.clone();
        info!("subscription_token_get(\"{}\") - X-Span-ID: {:?}", token, context.get().0.clone());
        Err("Generic failuare".into())
    }

    /// Return the server api version.
    async fn version_get(
        &self,
        context: &C) -> Result<VersionGetResponse, ApiError>
    {
        let context = context.clone();
        info!("version_get() - X-Span-ID: {:?}", context.get().0.clone());
        Err("Generic failuare".into())
    }

}
